package com.timetrakkr.service

import com.timetrakkr.entities.LeaveRecord

interface LeaveRecordService {
    fun createLeaveRecord(body: LeaveRecord, employeeId: Long): LeaveRecord
    fun approveLeaveRecord(recordId: Long, employeeId: Long): LeaveRecord
}