package service

import com.timetrakkr.entities.Employee

interface EmployeeService {
    fun createEmployee(body: Employee): Employee
    fun updateEmployee(body: Employee, id: Long): Employee
    fun toggleIsActive(id: Long): Employee
}