package com.timetrakkr.service

import com.timetrakkr.entities.TimeRecord
import java.time.LocalDate

interface TimeRecordService {
    fun createTimeRecord(body: TimeRecord, employeeId: Long): TimeRecord
    fun getTimeRecordsByEmployee(id: Long, startDate: LocalDate?= LocalDate.MIN,
                                 endDate: LocalDate?= LocalDate.MAX): List<TimeRecord>
    fun updateTimeRecord(body: TimeRecord, id: Long): TimeRecord
}