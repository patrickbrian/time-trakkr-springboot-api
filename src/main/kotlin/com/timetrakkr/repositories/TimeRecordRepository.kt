package com.timetrakkr.repositories


import com.timetrakkr.entities.TimeRecord
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query
import org.springframework.stereotype.Repository
import java.time.LocalDate

@Repository
interface TimeRecordRepository: JpaRepository<TimeRecord, Long> {

    @Query(
        """
            SELECT CASE WHEN COUNT(tr) >= 2 THEN TRUE ELSE FALSE END
            FROM TimeRecord tr WHERE tr.employee.id = :employeeId AND tr.dateRecord = :dateRecord
        """
    )
    fun doesTimeInExceeds2(dateRecord: LocalDate, employeeId: Long): Boolean

    @Query(
        """
            SELECT CASE WHEN COUNT(tr) > 0 THEN TRUE ELSE FALSE END
            FROM TimeRecord tr WHERE tr.employee.id = :employeeId AND tr.dateRecord = :dateRecord
        """
    )
    fun doesTimeInExists(dateRecord: LocalDate, employeeId: Long): Boolean
}