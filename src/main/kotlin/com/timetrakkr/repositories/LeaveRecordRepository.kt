package com.timetrakkr.repositories

import com.timetrakkr.entities.LeaveRecord
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query

interface LeaveRecordRepository: JpaRepository<LeaveRecord, Long> {
    @Query(
        """
            SELECT CASE WHEN COUNT(lr) < 5 THEN TRUE ELSE FALSE END
            FROM LeaveRecord lr 
            WHERE lr.employee.id = :employeeId 
             AND MONTH(lr.dateRecord) = :month 
             AND lr.leaveType = 'VACATION LEAVE'
        """
    )
    fun checkUnusedMonthlyVL(month: Int, employeeId: Long): Boolean

    @Query(
        """
            SELECT CASE WHEN COUNT(lr) < 2 THEN TRUE ELSE FALSE END
            FROM LeaveRecord lr 
            WHERE lr.employee.id = :employeeId 
             AND MONTH(lr.dateRecord) = :month
             AND lr.leaveType = 'SICK LEAVE'
        """
    )
    fun checkUnusedMonthlySL(month: Int, employeeId: Long): Boolean
}