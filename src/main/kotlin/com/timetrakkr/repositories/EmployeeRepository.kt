package com.timetrakkr.repositories

import com.timetrakkr.entities.Employee
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query
import org.springframework.stereotype.Repository

@Repository
interface EmployeeRepository: JpaRepository<Employee, Long> {

    @Query(
        """
            SELECT CASE WHEN COUNT(e) > 0 THEN TRUE ELSE FALSE END
            FROM Employee e WHERE e.firstName = :firstName
        """
    )
    fun doesFirstNameExists(firstName: String): Boolean

    @Query(
        """
            SELECT CASE WHEN COUNT(e) > 0 THEN TRUE ELSE FALSE END
            FROM Employee e WHERE e.lastName = :lastName
        """
    )
    fun doesLastNameExists(lastName: String): Boolean
}