package com.timetrakkr.utils

object Constants {
    const val MIN_DAYS_ADVANCE_FOR_FILING_LEAVE = 14
    const val MAX_TIME_RECORD_HOURS_IN_SECONDS = 36000
}