package com.timetrakkr.utils

enum class LeaveTypeEnum(val value: String) {
    VL("VACATION LEAVE"),
    BL("BUSINESS LEAVE"),
    SL("SICK LEAVE")
}