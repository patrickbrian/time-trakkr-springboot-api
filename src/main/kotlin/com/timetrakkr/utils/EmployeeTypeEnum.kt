package utils

enum class EmployeeTypeEnum(val value: String) {
    PM("PROJECT MANAGER"),
    C("CONTRACTOR")
}