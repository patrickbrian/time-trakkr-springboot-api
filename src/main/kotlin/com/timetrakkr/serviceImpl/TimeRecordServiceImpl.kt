package com.timetrakkr.serviceImpl

import java.time.temporal.ChronoUnit.SECONDS
import java.time.temporal.ChronoUnit.DAYS
import com.timetrakkr.entities.TimeRecord
import com.timetrakkr.repositories.EmployeeRepository
import com.timetrakkr.repositories.TimeRecordRepository
import com.timetrakkr.service.TimeRecordService
import com.timetrakkr.utils.Constants
import org.springframework.http.HttpStatus
import org.springframework.stereotype.Service
import org.springframework.web.server.ResponseStatusException
import java.time.LocalDate
import java.time.LocalTime

@Service
class TimeRecordServiceImpl(
    private val timeRecordRepository: TimeRecordRepository,
    private val employeeRepository: EmployeeRepository

) : TimeRecordService {

    /**
     * Throws an error for if:
     * 1. The employee does not exist given an id
     * 2. The employee already has 2 time-ins on the same date of the time record
     * 3. The time-in is before the time-out (validateTimeInRecord method)
     * 4. The difference between time-in and time-out is greater than 10 (validateTimeInRecord method)
     * 5. The record overlaps the other record on the same date (validateTimeInRecord method)
     *
     * Otherwise, save the time record
     */
    override fun createTimeRecord(body: TimeRecord, employeeId: Long): TimeRecord {

        val employee = employeeRepository.findById(employeeId).orElseThrow {
            ResponseStatusException(HttpStatus.NOT_FOUND, "Employee with id: $employeeId does not exist.")
        }

        if(timeRecordRepository.doesTimeInExceeds2(body.dateRecord, employeeId)){
            throw ResponseStatusException(HttpStatus.TOO_MANY_REQUESTS, "Employee with id: $employeeId " +
                    "already have 2 time-ins on ${body.dateRecord}!")
        }

        validateTimeInRecord(body, employeeId)

        return timeRecordRepository.save(body.copy(
            employee = employee
        ))
    }

    /**
     * Retrieves all employee's time record if given a date range,
     * otherwise, it will retrieve all time record if there's no given date range
     */
    override fun getTimeRecordsByEmployee(id: Long, startDate: LocalDate?, endDate: LocalDate?): List<TimeRecord> {

        if(startDate?.until(endDate, DAYS)!! <= 0 )
            throw ResponseStatusException(HttpStatus.FORBIDDEN, "Start date should be before end date.")

        val records = timeRecordRepository.findAll().filter {
            it.employee?.id == id && it.dateRecord >= startDate && it.dateRecord <= endDate
        }
        return records

    }

    /**
     * Throws an error for if:
     * 1. The time record does not exist given an id
     * 2. The time record was already updated
     * 3. The time-in is before the time-out (validateTimeInRecord method)
     * 4. The difference between time-in and time-out is greater than 10 (validateTimeInRecord method)
     * 5. The record overlaps the other record on the same date (validateTimeInRecord method)
     *
     * Otherwise, update the time record
     */
    override fun updateTimeRecord(body: TimeRecord, id: Long): TimeRecord {
        val record = timeRecordRepository.findById(id).orElseThrow {
            ResponseStatusException(HttpStatus.NOT_FOUND, "Time-in record with id: $id does not exist.")
        }

        if(record.isUpdated)
            throw ResponseStatusException(HttpStatus.FORBIDDEN, "Time-in record with id: $id was already updated" +
                    " once and is not allowed to be updated anymore.")

        validateTimeInRecord(body, record.employee?.id!!, id)

        return timeRecordRepository.save(record.copy(
            timeIn = body.timeIn,
            timeOut = body.timeOut,
            isUpdated = true,
            dateRecord = body.dateRecord
        ))
    }

    /**
     * Throws an error for if:
     * 1. The time-in is before the time-out
     * 2. The difference between time-in and time-out is greater than 10
     * 3. The record overlaps the other record on the same date (checkOverlaps method)
     */
    fun validateTimeInRecord(body: TimeRecord, employeeId: Long, recordId: Long? = null){

        if(body.timeIn >= body.timeOut)
            throw ResponseStatusException(HttpStatus.FORBIDDEN, "Time-in should be before time-out")
        else if(body.timeIn.until(body.timeOut, SECONDS) > Constants.MAX_TIME_RECORD_HOURS_IN_SECONDS)
            throw ResponseStatusException(HttpStatus.FORBIDDEN, "You can only record a max of 10 hours in one record.")
        else if(timeRecordRepository.doesTimeInExists(body.dateRecord, employeeId)){

            val previousRecord = timeRecordRepository.findAll().filter{ it.employee?.id == employeeId }

            if(recordId == previousRecord.first().id)
                checkOverlaps(body, previousRecord.last().timeIn, previousRecord.last().timeOut)
            else
                checkOverlaps(body, previousRecord.first().timeIn, previousRecord.first().timeOut)

        }
    }

    /**
     * Checks if the new record overlaps the other record on the same date
     */
    fun checkOverlaps(body: TimeRecord, previousTimeIn: LocalTime, previousTimeOut: LocalTime){

        //prevTimeIn - bodyTimeIn - prevTimeOut - bodyTimeOut
        //prevTimeIn - bodyTimeIn - bodyTimeOut - prevTimeout
        //bodyTimeIn - prevTimeIn - bodyTimeOut - prevTimeOut
        //bodyTimeIn - prevTimeIn - prevTimeOut - bodyTimeOut
        if(previousTimeIn.until(body.timeIn, SECONDS) >= 0
            && body.timeIn.until(previousTimeOut, SECONDS) >= 0
            && previousTimeOut.until(body.timeOut, SECONDS) >= 0
            ||
            previousTimeIn.until(body.timeIn, SECONDS) >= 0
            && body.timeIn.until(body.timeOut, SECONDS) >= 0
            && body.timeOut.until(previousTimeOut, SECONDS) >= 0
            ||
            body.timeIn.until(previousTimeIn, SECONDS) >= 0
            && previousTimeIn.until(body.timeOut, SECONDS) >= 0
            && body.timeOut.until(previousTimeOut, SECONDS) >= 0
            ||
            body.timeIn.until(previousTimeIn, SECONDS) >= 0
            && previousTimeIn.until(previousTimeOut, SECONDS) >= 0
            && previousTimeOut.until(body.timeOut, SECONDS) >= 0)
            throw ResponseStatusException(HttpStatus.CONFLICT, "Your new time-in record overlaps your other " +
                    "time-in record on ${body.dateRecord}!")
    }
}