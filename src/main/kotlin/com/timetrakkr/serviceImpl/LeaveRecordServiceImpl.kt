package com.timetrakkr.serviceImpl

import com.timetrakkr.entities.LeaveRecord
import com.timetrakkr.repositories.EmployeeRepository
import com.timetrakkr.repositories.LeaveRecordRepository

import com.timetrakkr.service.LeaveRecordService
import com.timetrakkr.utils.Constants
import com.timetrakkr.utils.LeaveTypeEnum
import org.springframework.http.HttpStatus
import org.springframework.stereotype.Service
import org.springframework.web.server.ResponseStatusException
import utils.EmployeeTypeEnum

import java.time.LocalDate
import java.time.temporal.ChronoUnit.DAYS

@Service
class LeaveRecordServiceImpl(
    private val leaveRecordRepository: LeaveRecordRepository,
    private val employeeRepository: EmployeeRepository
) : LeaveRecordService {

    /**
     * Throws an error for if:
     * 1. The employee does not exist given an id
     * 2. The dateRcord property is less than 14 than the current date
     * 3. There's no more available vacation leave for the month of the record
     * 4. There's no more available sick leave for the month of the record
     *
     * Otherwise, save the leave record
     */
    override fun createLeaveRecord(body: LeaveRecord, employeeId: Long): LeaveRecord {
        val employee = employeeRepository.findById(employeeId).orElseThrow {
            ResponseStatusException(HttpStatus.NOT_FOUND, "Employee with id: $employeeId does not exist.")
        }

        if(body.leaveType == "VL"){
            if(LocalDate.now().until(body.dateRecord, DAYS) < Constants.MIN_DAYS_ADVANCE_FOR_FILING_LEAVE)
                throw ResponseStatusException(HttpStatus.FORBIDDEN, "You can only file for a vacation leave " +
                        "14 days in advance.")

            if(!leaveRecordRepository.checkUnusedMonthlyVL(body.dateRecord.monthValue, employeeId))
                throw ResponseStatusException(HttpStatus.FORBIDDEN, "You already used your 5 vacation leave for" +
                        " the month of ${body.dateRecord.month} ${body.dateRecord.year}." )
        }else if(body.leaveType == "SL")
            if(!leaveRecordRepository.checkUnusedMonthlySL(body.dateRecord.monthValue, employeeId))
                throw ResponseStatusException(HttpStatus.FORBIDDEN, "You already used your 2 sick leave for" +
                        " the month of ${body.dateRecord.month} ${body.dateRecord.year}." )

        var leaveTypeEnum: LeaveTypeEnum
        try{
            leaveTypeEnum = LeaveTypeEnum.valueOf(body.leaveType.uppercase())
        }catch (e: IllegalArgumentException) {
            throw ResponseStatusException(HttpStatus.BAD_REQUEST, "Leave type: ${body.leaveType.uppercase()} " +
                    "does not exist.")
        }

        return leaveRecordRepository.save(body.copy(
            employee = employee,
            leaveType = leaveTypeEnum.value
        ))
    }

    /**
     * Throws an error for if:
     * 1. The employee does not exist given an id
     * 2. The leave record does exist given an id
     * 3. The employeeType property of the given employee id is not a PROJECT MANAGER
     * 4. The first and last name of the given employee matches the first and last name of the leave record
     *
     * Otherwise, it will change the isApproved property to true
     */
    override fun approveLeaveRecord(recordId: Long, employeeId: Long): LeaveRecord {
        val employee = employeeRepository.findById(employeeId).orElseThrow {
            ResponseStatusException(HttpStatus.NOT_FOUND, "Employee with id: $employeeId does not exist.")
        }

        val record = leaveRecordRepository.findById(recordId).orElseThrow{
            ResponseStatusException(HttpStatus.NOT_FOUND, "Leave record with id: $recordId does not exist.")
        }

        if(employee.employeeType != "PROJECT MANAGER")
            throw ResponseStatusException(HttpStatus.UNAUTHORIZED, "Only a Project Manager can approve a leave.")
        else if("${employee.firstName} ${employee.lastName}" == "${record.employee?.firstName} ${record.employee?.lastName}")
            throw ResponseStatusException(HttpStatus.FORBIDDEN, "You can not approve your own leave.")
        else if(record.isApproved)
            throw ResponseStatusException(HttpStatus.FORBIDDEN, "Leave record with id: $recordId was already approved.")

        return leaveRecordRepository.save(record.copy(
            isApproved = true
        ))

    }
}