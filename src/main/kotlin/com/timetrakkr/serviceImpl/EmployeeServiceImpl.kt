package com.timetrakkr.serviceImpl

import com.timetrakkr.entities.Employee
import org.springframework.http.HttpStatus
import org.springframework.stereotype.Service
import org.springframework.web.server.ResponseStatusException
import com.timetrakkr.repositories.EmployeeRepository
import service.EmployeeService
import utils.EmployeeTypeEnum

@Service
class EmployeeServiceImpl(
    private val employeeRepository: EmployeeRepository
) : EmployeeService {

    /**
     * Throws an error if the first and last name exists
     * and will save the data if there's no error.
     */
    override fun createEmployee(body: Employee): Employee {

        if(employeeRepository.doesFirstNameExists(body.firstName)
            && employeeRepository.doesLastNameExists(body.lastName)){

            throw ResponseStatusException(HttpStatus.CONFLICT,
                "The name: ${body.firstName} ${body.lastName} already exists!")
        }

        var employeeType: EmployeeTypeEnum

        try{
            employeeType = EmployeeTypeEnum.valueOf(body.employeeType.uppercase())
        }catch (e: IllegalArgumentException) {
            throw ResponseStatusException(HttpStatus.BAD_REQUEST, "Employee type: ${body.employeeType.uppercase()} " +
                    "does not exist.")
        }

        return employeeRepository.save(body.copy(
            employeeType = employeeType.value
        ))
    }

    /**
     * Throws an error if the employee does not exist given an id
     * as well as if the first and last name already exist.
     * Updates the data if all requirements are met.
     */
    override fun updateEmployee(body: Employee, id: Long): Employee {
        val employee = employeeRepository.findById(id).orElseThrow {
            ResponseStatusException(HttpStatus.NOT_FOUND, "Employee with id: $id does not exist.")
        }

        if(employeeRepository.doesFirstNameExists(body.firstName)
            && employeeRepository.doesLastNameExists(body.lastName)){

            throw ResponseStatusException(HttpStatus.CONFLICT,
                "The name: ${body.firstName} ${body.lastName} already exists!")
        }

        return employeeRepository.save(employee.copy(
            firstName = body.firstName,
            lastName = body.lastName,
        ))
    }

    /**
     * Throws an error if the employee does not exist given an id
     * and will toggle the isActive property if there's no error.
     */
    override fun toggleIsActive(id: Long): Employee {
        val employee = employeeRepository.findById(id).orElseThrow {
            ResponseStatusException(HttpStatus.NOT_FOUND, "Employee with id: $id does not exist.")
        }

        return employeeRepository.save(employee.copy(
            isActive = !employee.isActive
        ))
    }
}