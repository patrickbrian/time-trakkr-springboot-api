package com.timetrakkr.entities


import java.time.LocalDate
import java.time.LocalTime
import javax.persistence.*

@Entity(name = "TimeRecord")
@Table(name = "timeRecords")
data class TimeRecord(
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    val id: Long? = null, // Long -> BIGINT

    @Column(
        nullable = false,
        updatable = true,
        name = "timeIn"
    )
    var timeIn: LocalTime,

    @Column(
        nullable = false,
        updatable = true,
        name = "timeOut"
    )
    var timeOut: LocalTime,

    @Column(
        nullable = false,
        updatable = true,
        name = "isUpdated"
    )
    var isUpdated: Boolean,

    @Column(
        nullable = false,
        updatable = true,
        name = "dateRecord"
    )
    var dateRecord: LocalDate,

    @ManyToOne(fetch = FetchType.LAZY, optional = true)
    @JoinColumn(nullable = true)
    var employee: Employee? = null
)
