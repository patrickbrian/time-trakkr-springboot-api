package com.timetrakkr.entities

import java.time.LocalDate
import javax.persistence.*

@Entity(name = "LeaveRecord")
@Table(name = "leaveRecords")
data class LeaveRecord(
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    val id: Long? = null, // Long -> BIGINT

    @Column(
        nullable = false,
        updatable = true,
        name = "leaveType"
    )
    var leaveType: String,

    @Column(
        nullable = false,
        updatable = true,
        name = "dateRecord"
    )
    var dateRecord: LocalDate,

    @Column(
        nullable = false,
        updatable = true,
        name = "isApproved"
    )
    var isApproved: Boolean = false,

    @ManyToOne(fetch = FetchType.LAZY, optional = true)
    @JoinColumn(nullable = true)
    var employee: Employee? = null
)
