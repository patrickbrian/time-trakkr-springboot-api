package com.timetrakkr.entities

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import javax.persistence.*

@Entity(name = "Employee")
@Table(name = "employees")
data class Employee(
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    val id: Long? = null, // Long -> BIGINT

    @Column(
        nullable = false,
        updatable = true,
        name = "firstName"
    )
    var firstName: String,

    @Column(
        nullable = false,
        updatable = true,
        name = "lastName"
    )
    var lastName: String,

    @Column(
        nullable = false,
        updatable = true,
        name = "employeeType"
    )
    var employeeType: String,

    @Column(
        nullable = false,
        updatable = true,
        name = "isActive"
    )
    var isActive: Boolean = true,

    @JsonIgnoreProperties(value = ["employee"], allowSetters = true)
    @OneToMany(mappedBy = "employee", fetch = FetchType.LAZY)
    val timeRecords: List<TimeRecord> = mutableListOf(),

    @JsonIgnoreProperties(value = ["employee"], allowSetters = true)
    @OneToMany(mappedBy = "employee", fetch = FetchType.LAZY)
    val leaveRecords: List<LeaveRecord> = mutableListOf(),
)
