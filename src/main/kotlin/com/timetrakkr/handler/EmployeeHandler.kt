package com.timetrakkr.handler

import com.timetrakkr.entities.Employee
import org.springframework.web.bind.annotation.*
import service.EmployeeService

@RestController
class EmployeeHandler(
    private val employeeService: EmployeeService
) {
    @PostMapping("/api/employees/")
    fun createEmployee(@RequestBody employee: Employee): Employee =
        employeeService.createEmployee(employee)

    @PutMapping("/api/employee/{id}/")
    fun updateEmployee(@RequestBody body: Employee, @PathVariable("id") id: Long): Employee =
        employeeService.updateEmployee(body, id)

    //api/employee/{id}/isActive
    @PutMapping("/api/employee/{id}/isActive/")
    fun toggleIsActive(@PathVariable("id") id: Long): Employee =
        employeeService.toggleIsActive(id)

}