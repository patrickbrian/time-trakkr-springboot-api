package com.timetrakkr.handler

import com.timetrakkr.entities.TimeRecord
import com.timetrakkr.service.TimeRecordService
import org.springframework.format.annotation.DateTimeFormat
import org.springframework.web.bind.annotation.*
import java.time.LocalDate
import javax.websocket.server.PathParam

@RestController
class TimeRecordHandler(

    private val timeRecordService: TimeRecordService
)  {
    @PostMapping("/api/time-records/{employeeId}/")
    fun createTimeRecord(
        @RequestBody timeRecord: TimeRecord,
        @PathVariable employeeId: Long
    ): TimeRecord = timeRecordService.createTimeRecord(timeRecord, employeeId)

    @GetMapping("/api/time-records/{employeeId}/")
    fun getTimeRecordsByEmployee(
        @PathVariable("employeeId") employeeId: Long,
        @PathParam("startDate") @DateTimeFormat(pattern = "yyyy-MM-dd") startDate: LocalDate? = LocalDate.MIN,
        @PathParam("endDate") @DateTimeFormat(pattern = "yyyy-MM-dd") endDate: LocalDate? = LocalDate.MAX
    ): List<TimeRecord> = timeRecordService.getTimeRecordsByEmployee(
        employeeId, startDate?:LocalDate.MIN, endDate?:LocalDate.MAX)

    @PutMapping("/api/time-record/{recordId}/")
    fun updateTimeRecord(
        @PathVariable("recordId") recordId: Long,
        @RequestBody body: TimeRecord
    ): TimeRecord = timeRecordService.updateTimeRecord(body, recordId)

}