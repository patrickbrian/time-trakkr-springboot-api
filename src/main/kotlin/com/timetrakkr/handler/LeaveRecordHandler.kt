package com.timetrakkr.handler

import com.timetrakkr.entities.Employee
import com.timetrakkr.entities.LeaveRecord
import com.timetrakkr.entities.TimeRecord
import com.timetrakkr.service.LeaveRecordService
import org.springframework.web.bind.annotation.*

@RestController
class LeaveRecordHandler(
    val leaveRecordService: LeaveRecordService
) {
    @PostMapping("/api/leave-records/{employeeId}/")
    fun createLeaveRecord(
        @RequestBody leaveRecord: LeaveRecord,
        @PathVariable employeeId: Long
    ): LeaveRecord = leaveRecordService.createLeaveRecord(leaveRecord, employeeId)

    @PutMapping("/api/leaveRecord/isApproved/{recordId}/{employeeId}/")
    fun approveLeaveRecord(
        @PathVariable("recordId") recordId: Long,
        @PathVariable("employeeId") employeeId: Long
    ): LeaveRecord = leaveRecordService.approveLeaveRecord(recordId,employeeId)
}