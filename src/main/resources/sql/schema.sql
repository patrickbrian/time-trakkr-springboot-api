CREATE TABLE IF NOT EXISTS employees(
    id BIGINT AUTO_INCREMENT NOT NULL,
    firstName VARCHAR(255) NOT NULL,
    lastName VARCHAR(255) NOT NULL,
    employeeType VARCHAR(255) NOT NULL,
    isActive BIT DEFAULT 1 NOT NULL,
    PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS timeRecords(
    id BIGINT AUTO_INCREMENT NOT NULL,
    timeIn TIME NOT NULL,
    timeOut TIME NOT NULL,
    isUpdated BIT NOT NULL,
    dateRecord DATE NOT NULL,
    employeeId BIGINT NOT NULL,
    PRIMARY KEY (id),
    FOREIGN KEY (employeeId) REFERENCES employees(id)
);


CREATE TABLE IF NOT EXISTS leaveRecords(
    id BIGINT AUTO_INCREMENT NOT NULL,
    leaveType VARCHAR(255) NOT NULL,
    dateRecord DATE NOT NULL,
    isApproved BIT DEFAULT 0 NOT NULL,
    employeeId BIGINT NOT NULL,
    PRIMARY KEY (id),
    FOREIGN KEY (employeeId) REFERENCES employees(id)
);