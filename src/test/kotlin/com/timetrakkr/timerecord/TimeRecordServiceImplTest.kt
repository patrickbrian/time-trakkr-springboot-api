package com.timetrakkr.timerecord

import com.timetrakkr.TimeTrakkrApplicationTests
import com.timetrakkr.entities.TimeRecord
import com.timetrakkr.repositories.EmployeeRepository
import com.timetrakkr.repositories.TimeRecordRepository
import com.timetrakkr.serviceImpl.TimeRecordServiceImpl
import com.timetrakkr.utils.EntityGenerator
import org.assertj.core.api.Assertions
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.server.ResponseStatusException
import java.time.LocalDate
import java.time.LocalTime
import javax.transaction.Transactional
import kotlin.test.assertEquals
import kotlin.test.assertFailsWith

class TimeRecordServiceImplTest: TimeTrakkrApplicationTests() {

    @Autowired
    private lateinit var employeeRepository: EmployeeRepository

    @Autowired
    private lateinit var timeRecordRepository: TimeRecordRepository

    @Autowired
    private lateinit var timeRecordServiceImpl: TimeRecordServiceImpl

    @BeforeEach
    fun setUp(){
        timeRecordRepository.deleteAll()
        employeeRepository.deleteAll()
    }

    @Transactional
    @Test
    fun `create should return a time record`(){
        val employee = EntityGenerator.createEmployee()
        val createdEmployee = employeeRepository.save(employee)

        assertEquals(0, timeRecordRepository.findAll().count())
        val timeRecord = EntityGenerator.createTimeRecord().copy(
            employee = createdEmployee
        )
        val createdTimeRecord = timeRecordServiceImpl.createTimeRecord(timeRecord, createdEmployee.id!!)

        assertEquals(1, timeRecordRepository.findAll().count())

        Assertions.assertThat( createdTimeRecord )
            .usingRecursiveComparison()
            .ignoringFields(
                TimeRecord::id.name
            )
            .withStrictTypeChecking()
            .isEqualTo(timeRecord)

        assertEquals(timeRecord.timeIn, createdTimeRecord.timeIn)
        assertEquals(timeRecord.timeOut, createdTimeRecord.timeOut)
        assertEquals(timeRecord.isUpdated, createdTimeRecord.isUpdated)
        assertEquals(timeRecord.dateRecord, createdTimeRecord.dateRecord)
    }

    @Test
    fun `create should fail given a non existing employee id`(){
        val employee = EntityGenerator.createEmployee()
        val createdEmployee = employeeRepository.save(employee)

        val timeRecord = EntityGenerator.createTimeRecord().copy( employee = createdEmployee)

        val invalidId: Long = 123
        val exception = assertFailsWith<ResponseStatusException> {
            timeRecordServiceImpl.createTimeRecord(timeRecord, invalidId)
        }

        val expectedException = "404 NOT_FOUND \"Employee with id: $invalidId does not exist.\""
        assertEquals(expectedException, exception.message)
    }

    @Test
    fun `create should fail if there's already 2 time-ins on the same date`(){
        val employee = EntityGenerator.createEmployee()
        val createdEmployee = employeeRepository.save(employee)

        val timeRecord = EntityGenerator.createTimeRecord().copy( employee = createdEmployee)
        timeRecordRepository.saveAll(
            listOf(
                timeRecord,
                timeRecord.copy(
                    timeIn = LocalTime.parse("12:00:00"),
                    timeOut = LocalTime.parse("15:00:00"),
                    employee = employee
                )
            )
        )

        val thirdRecord = timeRecord.copy(
            timeIn = LocalTime.parse("16:00:00"),
            timeOut = LocalTime.parse("21:00:00"),
        )

        val exception = assertFailsWith<ResponseStatusException> {
            timeRecordServiceImpl.createTimeRecord(thirdRecord, createdEmployee.id!!)
        }

        val expectedException = "429 TOO_MANY_REQUESTS \"Employee with id: ${createdEmployee.id} " +
                "already have 2 time-ins on ${timeRecord.dateRecord}!\""
        assertEquals(expectedException, exception.message)

    }

    @Test
    fun `create should fail if time-out is before time-in`(){
        val employee = EntityGenerator.createEmployee()
        val createdEmployee = employeeRepository.save(employee)

        val timeRecord = EntityGenerator.createTimeRecord().copy(
            timeIn = LocalTime.parse("11:00:00"),
            timeOut = LocalTime.parse("10:59:59"),
            employee = createdEmployee
        )

        val exception = assertFailsWith<ResponseStatusException> {
            timeRecordServiceImpl.createTimeRecord(timeRecord, createdEmployee.id!!)
        }

        val expectedException = "403 FORBIDDEN \"Time-in should be before time-out\""
        assertEquals(expectedException, exception.message)

    }

    @Test
    fun `create should fail if the time record was more than 10 hrs`(){
        val employee = EntityGenerator.createEmployee()
        val createdEmployee = employeeRepository.save(employee)

        val timeRecord = EntityGenerator.createTimeRecord().copy(
            timeIn = LocalTime.parse("01:00:00"),
            timeOut = LocalTime.parse("11:00:01"),
            employee = createdEmployee
        )

        val exception = assertFailsWith<ResponseStatusException> {
            timeRecordServiceImpl.createTimeRecord(timeRecord, createdEmployee.id!!)
        }

        val expectedException = "403 FORBIDDEN \"You can only record a max of 10 hours in one record.\""
        assertEquals(expectedException, exception.message)
    }

    @Test
    fun `create should fail if the time record overlaps another time record`(){
        val employee = EntityGenerator.createEmployee()
        val createdEmployee = employeeRepository.save(employee)

        val timeRecord = EntityGenerator.createTimeRecord().copy( employee = createdEmployee)
        timeRecordRepository.save(timeRecord)

        val overlapRecord = timeRecord.copy(
            timeIn = LocalTime.parse("11:00:00"),
            timeOut = LocalTime.parse("21:00:00"),
        )
        //end 11:00:01
        //start 11:00:01

        val exception = assertFailsWith<ResponseStatusException> {
            timeRecordServiceImpl.createTimeRecord(overlapRecord, createdEmployee.id!!)
        }

        val expectedException = "409 CONFLICT \"Your new time-in record overlaps your other time-in " +
                "record on ${timeRecord.dateRecord}!\""
        assertEquals(expectedException, exception.message)
    }

    @Test
    fun `get time records given an employee id should return all employee's record if there's no given date range`(){
        val employee = EntityGenerator.createEmployee()
        val createdEmployee = employeeRepository.save(employee)

        val timeRecord = EntityGenerator.createTimeRecord().copy( employee = createdEmployee)
        timeRecordRepository.saveAll(
            listOf(
                timeRecord,
                timeRecord.copy( dateRecord = LocalDate.parse("2020-03-01"), employee = employee ),
                timeRecord.copy( dateRecord = LocalDate.parse("2020-04-01"), employee = employee ),
                timeRecord.copy( dateRecord = LocalDate.parse("2020-05-01"), employee = employee ),
                timeRecord.copy( dateRecord = LocalDate.parse("2020-06-01"), employee = employee ),
            )
        )

        val timeRecords = timeRecordServiceImpl.getTimeRecordsByEmployee(createdEmployee.id!!)

        assertEquals(5, timeRecords.count())
    }

    @Test
    fun `get time records given an employee id should return all employee's record between the given date range`(){
        val employee = EntityGenerator.createEmployee()
        val createdEmployee = employeeRepository.save(employee)

        val timeRecord = EntityGenerator.createTimeRecord()
        timeRecordRepository.saveAll(
            listOf(
                timeRecord.copy( dateRecord = LocalDate.parse("2020-02-01"), employee = createdEmployee ),
                timeRecord.copy( dateRecord = LocalDate.parse("2020-03-01"), employee = createdEmployee ),
                timeRecord.copy( dateRecord = LocalDate.parse("2020-04-01"), employee = createdEmployee ),
                timeRecord.copy( dateRecord = LocalDate.parse("2020-05-01"), employee = createdEmployee ),
                timeRecord.copy( dateRecord = LocalDate.parse("2020-06-01"), employee = createdEmployee ),
            )
        )

        val timeRecords = timeRecordServiceImpl.getTimeRecordsByEmployee(
            createdEmployee.id!!,
            LocalDate.parse("2020-01-01"),
            LocalDate.parse("2020-04-01")
        )
        assertEquals(3, timeRecords.count())
    }

    @Test
    fun `get time records given an employee id should fail if the start date is not before end date`(){
        val employee = EntityGenerator.createEmployee()
        val createdEmployee = employeeRepository.save(employee)

        val timeRecord = EntityGenerator.createTimeRecord()
        timeRecordRepository.save(timeRecord)

        val exception = assertFailsWith<ResponseStatusException> {
            timeRecordServiceImpl.getTimeRecordsByEmployee(
                createdEmployee.id!!,
                LocalDate.parse("2020-02-01"),
                LocalDate.parse("2020-01-01")
            )
        }

        val expectedException = "403 FORBIDDEN \"Start date should be before end date.\""
        assertEquals(expectedException, exception.message)

    }

    @Test
    fun `update time record should return updated given valid inputs`(){
        val employee = EntityGenerator.createEmployee()
        val createdEmployee = employeeRepository.save(employee)

        val timeRecord = EntityGenerator.createTimeRecord().copy( employee = createdEmployee)
        val createdRecord = timeRecordRepository.save(timeRecord)

        val body = createdRecord.copy(
            timeIn = LocalTime.parse("02:00:00"),
            timeOut = LocalTime.parse("12:00:00"),
            isUpdated = true,
            dateRecord = LocalDate.parse("2021-01-01")
        )

        val updateTimeRecord = timeRecordServiceImpl.updateTimeRecord(body, createdRecord.id!!)

        assertEquals(body.timeIn, updateTimeRecord.timeIn)
        assertEquals(body.timeOut, updateTimeRecord.timeOut)
        assertEquals(body.isUpdated, updateTimeRecord.isUpdated)
        assertEquals(body.dateRecord, updateTimeRecord.dateRecord)
    }

    @Test
    fun `update time record should fail given a non existing time-in record id`(){
        val employee = EntityGenerator.createEmployee()
        val createdEmployee = employeeRepository.save(employee)

        val timeRecord = EntityGenerator.createTimeRecord().copy( employee = createdEmployee)
        val createdRecord = timeRecordRepository.save(timeRecord)

        val body = createdRecord.copy(
            timeIn = LocalTime.parse("02:00:00"),
            timeOut = LocalTime.parse("12:00:00"),
            isUpdated = true,
            dateRecord = LocalDate.parse("2021-01-01")
        )

        val invalidId: Long = 123
        val exception = assertFailsWith<ResponseStatusException> {
            timeRecordServiceImpl.updateTimeRecord(body, invalidId)
        }

        val expectedException = "404 NOT_FOUND \"Time-in record with id: $invalidId does not exist.\""
        assertEquals(expectedException, exception.message)
    }

    @Test
    fun `update time record should fail if the time-in record was already updated`(){
        val employee = EntityGenerator.createEmployee()
        val createdEmployee = employeeRepository.save(employee)

        val timeRecord = EntityGenerator.createTimeRecord().copy(
            isUpdated = true,
            employee = createdEmployee
        )
        val createdRecord = timeRecordRepository.save(timeRecord)

        val body = createdRecord.copy(
            timeIn = LocalTime.parse("02:00:00"),
            timeOut = LocalTime.parse("12:00:00"),
            isUpdated = true,
            dateRecord = LocalDate.parse("2021-01-01")
        )

        val exception = assertFailsWith<ResponseStatusException> {
            timeRecordServiceImpl.updateTimeRecord(body, createdRecord.id!!)
        }

        val expectedException = "403 FORBIDDEN \"Time-in record with id: ${createdRecord.id} was " +
                "already updated once and is not allowed to be updated anymore.\""
        assertEquals(expectedException, exception.message)
    }

    @Test
    fun `update time record should fail if time-out is before time-in`(){
        val employee = EntityGenerator.createEmployee()
        val createdEmployee = employeeRepository.save(employee)

        val timeRecord = EntityGenerator.createTimeRecord().copy(
            employee = createdEmployee
        )
        val createdRecord = timeRecordRepository.save(timeRecord)

        val body = createdRecord.copy(
            timeIn = LocalTime.parse("12:00:00"),
            timeOut = LocalTime.parse("11:59:48"),
            isUpdated = true,
            dateRecord = LocalDate.parse("2021-01-01")
        )

        val exception = assertFailsWith<ResponseStatusException> {
            timeRecordServiceImpl.updateTimeRecord(body, createdRecord.id!!)
        }

        val expectedException = "403 FORBIDDEN \"Time-in should be before time-out\""
        assertEquals(expectedException, exception.message)
    }

    @Test
    fun `update time record should fail if the time record was more than 10 hrs`(){
        val employee = EntityGenerator.createEmployee()
        val createdEmployee = employeeRepository.save(employee)

        val timeRecord = EntityGenerator.createTimeRecord().copy(
            employee = createdEmployee
        )
        val createdRecord = timeRecordRepository.save(timeRecord)

        val body = createdRecord.copy(
            timeIn = LocalTime.parse("01:00:00"),
            timeOut = LocalTime.parse("11:00:01"),
            isUpdated = true,
            dateRecord = LocalDate.parse("2021-01-01")
        )

        val exception = assertFailsWith<ResponseStatusException> {
            timeRecordServiceImpl.updateTimeRecord(body, createdRecord.id!!)
        }

        val expectedException = "403 FORBIDDEN \"You can only record a max of 10 hours in one record.\""
        assertEquals(expectedException, exception.message)
    }

    @Test
    fun `update time record should fail if the time record overlaps another time record`(){
        val employee = EntityGenerator.createEmployee()
        val createdEmployee = employeeRepository.save(employee)

        val timeRecord = EntityGenerator.createTimeRecord().copy(
            employee = createdEmployee
        )
        val createdRecord = timeRecordRepository.save(timeRecord)

        timeRecordRepository.save(createdRecord.copy(
            timeIn = LocalTime.parse("12:00:00"),
            timeOut = LocalTime.parse("15:00:00"),
        ))

        val body = createdRecord.copy(
            timeIn = LocalTime.parse("15:00:00"),
            timeOut = LocalTime.parse("16:00:00"),
            isUpdated = true,
            dateRecord = LocalDate.parse("2022-02-01")
        )

        val exception = assertFailsWith<ResponseStatusException> {
            timeRecordServiceImpl.updateTimeRecord(body, createdRecord.id!!)
        }

        val expectedException = "409 CONFLICT \"Your new time-in record overlaps your other time-in " +
                "record on ${timeRecord.dateRecord}!\""
        assertEquals(expectedException, exception.message)
    }



}