package com.timetrakkr.timerecord

import com.fasterxml.jackson.databind.ObjectMapper
import com.timetrakkr.TimeTrakkrApplicationTests
import com.timetrakkr.repositories.EmployeeRepository
import com.timetrakkr.repositories.TimeRecordRepository
import com.timetrakkr.utils.EntityGenerator
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.http.MediaType
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.get
import org.springframework.test.web.servlet.post
import org.springframework.test.web.servlet.put
import java.time.LocalDate
import java.time.LocalTime

@AutoConfigureMockMvc
class TimeRecordServiceIntegrationTest: TimeTrakkrApplicationTests()  {
    @Autowired
    private lateinit var mockMvc: MockMvc

    @Autowired
    private lateinit var objectMapper: ObjectMapper

    @Autowired
    private lateinit var employeeRepository: EmployeeRepository

    @Autowired
    private lateinit var timeRecordRepository: TimeRecordRepository

    @BeforeEach
    fun setUp(){
        timeRecordRepository.deleteAll()
        employeeRepository.deleteAll()
    }

    @Test
    fun `create should return 200`(){
        val employee = EntityGenerator.createEmployee()
        val createdEmployee = employeeRepository.save(employee)

        val timeRecord = EntityGenerator.createTimeRecord().copy(
            employee = createdEmployee
        )


        mockMvc.post("/api/time-records/${createdEmployee.id}/"){
            accept(MediaType.APPLICATION_JSON)
            contentType = MediaType.APPLICATION_JSON
            content = objectMapper.writeValueAsString(timeRecord)
        }.andExpect {
            status { isOk() }
        }
    }

    @Test
    fun `create should fail given a non existing employee id`(){
        val employee = EntityGenerator.createEmployee()
        val createdEmployee = employeeRepository.save(employee)

        val timeRecord = EntityGenerator.createTimeRecord().copy( employee = createdEmployee)

        val invalidId: Long = 123
        mockMvc.post("/api/time-records/$invalidId/"){
            accept(MediaType.APPLICATION_JSON)
            contentType = MediaType.APPLICATION_JSON
            content = objectMapper.writeValueAsString(timeRecord)
        }.andExpect {
            status { isNotFound() }
        }
    }

    @Test
    fun `create should fail if there's already 2 time-ins on the same date`(){
        val employee = EntityGenerator.createEmployee()
        val createdEmployee = employeeRepository.save(employee)

        val timeRecord = EntityGenerator.createTimeRecord().copy( employee = createdEmployee)
        timeRecordRepository.saveAll(
            listOf(
                timeRecord,
                timeRecord.copy(
                    timeIn = LocalTime.parse("12:00:00"),
                    timeOut = LocalTime.parse("15:00:00"),
                    employee = employee
                )
            )
        )

        val thirdRecord = timeRecord.copy(
            timeIn = LocalTime.parse("16:00:00"),
            timeOut = LocalTime.parse("21:00:00"),
        )
        mockMvc.post("/api/time-records/${createdEmployee.id}/"){
            accept(MediaType.APPLICATION_JSON)
            contentType = MediaType.APPLICATION_JSON
            content = objectMapper.writeValueAsString(thirdRecord)
        }.andExpect {
            status { isTooManyRequests() }
        }
    }

    @Test
    fun `create should fail if time-out is before time-in`(){
        val employee = EntityGenerator.createEmployee()
        val createdEmployee = employeeRepository.save(employee)

        val timeRecord = EntityGenerator.createTimeRecord().copy(
            timeIn = LocalTime.parse("11:00:00"),
            timeOut = LocalTime.parse("10:59:59"),
            employee = createdEmployee
        )

        mockMvc.post("/api/time-records/${createdEmployee.id}/"){
            accept(MediaType.APPLICATION_JSON)
            contentType = MediaType.APPLICATION_JSON
            content = objectMapper.writeValueAsString(timeRecord)
        }.andExpect {
            status { isForbidden() }
        }
    }

    @Test
    fun `create should fail if the time record was more than 10 hrs`(){
        val employee = EntityGenerator.createEmployee()
        val createdEmployee = employeeRepository.save(employee)

        val timeRecord = EntityGenerator.createTimeRecord().copy(
            timeIn = LocalTime.parse("01:00:00"),
            timeOut = LocalTime.parse("11:00:01"),
            employee = createdEmployee
        )


        mockMvc.post("/api/time-records/${createdEmployee.id}/"){
            accept(MediaType.APPLICATION_JSON)
            contentType = MediaType.APPLICATION_JSON
            content = objectMapper.writeValueAsString(timeRecord)
        }.andExpect {
            status { isForbidden() }
        }
    }

    @Test
    fun `create should fail if the time record overlaps another time record`(){
        val employee = EntityGenerator.createEmployee()
        val createdEmployee = employeeRepository.save(employee)

        val timeRecord = EntityGenerator.createTimeRecord().copy( employee = createdEmployee)
        timeRecordRepository.save(timeRecord)

        val overlapRecord = timeRecord.copy(
            timeIn = LocalTime.parse("11:00:00"),
            timeOut = LocalTime.parse("21:00:00"),
        )
        mockMvc.post("/api/time-records/${createdEmployee.id}/"){
            accept(MediaType.APPLICATION_JSON)
            contentType = MediaType.APPLICATION_JSON
            content = objectMapper.writeValueAsString(overlapRecord)
        }.andExpect {
            status { isConflict() }
        }
    }

    @Test
    fun `get time records given an employee id should return all employee's record if there's no given date range`(){
        val employee = EntityGenerator.createEmployee()
        val createdEmployee = employeeRepository.save(employee)

        val timeRecord = EntityGenerator.createTimeRecord().copy( employee = createdEmployee)
        timeRecordRepository.saveAll(
            listOf(
                timeRecord,
                timeRecord.copy( dateRecord = LocalDate.parse("2020-03-01"), employee = employee ),
                timeRecord.copy( dateRecord = LocalDate.parse("2020-04-01"), employee = employee ),
                timeRecord.copy( dateRecord = LocalDate.parse("2020-05-01"), employee = employee ),
                timeRecord.copy( dateRecord = LocalDate.parse("2020-06-01"), employee = employee ),
            )
        )

        mockMvc.get("/api/time-records/${createdEmployee.id}/"){
            accept(MediaType.APPLICATION_JSON)
            contentType = MediaType.APPLICATION_JSON
        }.andExpect {
            status { isOk() }
            jsonPath("$.length()") { value(5) }
        }
    }

    @Test
    fun `get time records given an employee id should return all employee's record between the given date range`(){
        val employee = EntityGenerator.createEmployee()
        val createdEmployee = employeeRepository.save(employee)

        val timeRecord = EntityGenerator.createTimeRecord()
        timeRecordRepository.saveAll(
            listOf(
                timeRecord.copy( dateRecord = LocalDate.parse("2020-02-01"), employee = createdEmployee ),
                timeRecord.copy( dateRecord = LocalDate.parse("2020-03-01"), employee = createdEmployee ),
                timeRecord.copy( dateRecord = LocalDate.parse("2020-04-01"), employee = createdEmployee ),
                timeRecord.copy( dateRecord = LocalDate.parse("2020-05-01"), employee = createdEmployee ),
                timeRecord.copy( dateRecord = LocalDate.parse("2020-06-01"), employee = createdEmployee ),
            )
        )

        mockMvc.get("/api/time-records/${createdEmployee.id}/?startDate=2020-01-01&endDate=2020-04-01"){
            accept(MediaType.APPLICATION_JSON)
            contentType = MediaType.APPLICATION_JSON
        }.andExpect {
            status { isOk() }
            jsonPath("$.length()") { value(3) }
        }
    }

    @Test
    fun `get time records given an employee id should fail if the start date is not before end date`(){
        val employee = EntityGenerator.createEmployee()
        val createdEmployee = employeeRepository.save(employee)

        val timeRecord = EntityGenerator.createTimeRecord()
        timeRecordRepository.save(timeRecord)

        mockMvc.get("/api/time-records/${createdEmployee.id}/?startDate=2020-02-01&endDate=2020-01-01"){
            accept(MediaType.APPLICATION_JSON)
            contentType = MediaType.APPLICATION_JSON
        }.andExpect {
            status { isForbidden() }
        }
    }

    @Test
    fun `update time record should return updated given valid inputs`(){
        val employee = EntityGenerator.createEmployee()
        val createdEmployee = employeeRepository.save(employee)

        val timeRecord = EntityGenerator.createTimeRecord().copy( employee = createdEmployee)
        val createdRecord = timeRecordRepository.save(timeRecord)

        val body = createdRecord.copy(
            timeIn = LocalTime.parse("02:00:00"),
            timeOut = LocalTime.parse("12:00:00"),
            isUpdated = true,
            dateRecord = LocalDate.parse("2021-01-01")
        )

        mockMvc.put("/api/time-record/${createdRecord.id}/"){
            accept(MediaType.APPLICATION_JSON)
            contentType = MediaType.APPLICATION_JSON
            content = objectMapper.writeValueAsString(body)
        }.andExpect {
            status { isOk() }
        }
    }

    @Test
    fun `update time record should fail given a non existing time-in record id`(){
        val employee = EntityGenerator.createEmployee()
        val createdEmployee = employeeRepository.save(employee)

        val timeRecord = EntityGenerator.createTimeRecord().copy( employee = createdEmployee)
        val createdRecord = timeRecordRepository.save(timeRecord)

        val body = createdRecord.copy(
            timeIn = LocalTime.parse("02:00:00"),
            timeOut = LocalTime.parse("12:00:00"),
            isUpdated = true,
            dateRecord = LocalDate.parse("2021-01-01")
        )

        val invalidId: Long = 123
        mockMvc.put("/api/time-record/$invalidId"){
            accept(MediaType.APPLICATION_JSON)
            contentType = MediaType.APPLICATION_JSON
            content = objectMapper.writeValueAsString(body)
        }.andExpect {
            status { isNotFound() }
        }

    }

    @Test
    fun `update time record should fail if the time-in record was already updated`(){
        val employee = EntityGenerator.createEmployee()
        val createdEmployee = employeeRepository.save(employee)

        val timeRecord = EntityGenerator.createTimeRecord().copy(
            isUpdated = true,
            employee = createdEmployee
        )
        val createdRecord = timeRecordRepository.save(timeRecord)

        val body = createdRecord.copy(
            timeIn = LocalTime.parse("02:00:00"),
            timeOut = LocalTime.parse("12:00:00"),
            isUpdated = true,
            dateRecord = LocalDate.parse("2021-01-01")
        )
        mockMvc.put("/api/time-record/${createdRecord.id}/"){
            accept(MediaType.APPLICATION_JSON)
            contentType = MediaType.APPLICATION_JSON
            content = objectMapper.writeValueAsString(body)
        }.andExpect {
            status { isForbidden() }
        }
    }

    @Test
    fun `update time record should fail if time-out is before time-in`(){
        val employee = EntityGenerator.createEmployee()
        val createdEmployee = employeeRepository.save(employee)

        val timeRecord = EntityGenerator.createTimeRecord().copy(
            employee = createdEmployee
        )
        val createdRecord = timeRecordRepository.save(timeRecord)

        val body = createdRecord.copy(
            timeIn = LocalTime.parse("12:00:00"),
            timeOut = LocalTime.parse("11:59:48"),
            isUpdated = true,
            dateRecord = LocalDate.parse("2021-01-01")
        )
        mockMvc.put("/api/time-record/${createdRecord.id}/"){
            accept(MediaType.APPLICATION_JSON)
            contentType = MediaType.APPLICATION_JSON
            content = objectMapper.writeValueAsString(body)
        }.andExpect {
            status { isForbidden() }
        }
    }

    @Test
    fun `update time record should fail if the time record was more than 10 hrs`(){
        val employee = EntityGenerator.createEmployee()
        val createdEmployee = employeeRepository.save(employee)

        val timeRecord = EntityGenerator.createTimeRecord().copy(
            employee = createdEmployee
        )
        val createdRecord = timeRecordRepository.save(timeRecord)

        val body = createdRecord.copy(
            timeIn = LocalTime.parse("01:00:00"),
            timeOut = LocalTime.parse("11:00:01"),
            isUpdated = true,
            dateRecord = LocalDate.parse("2021-01-01")
        )
        mockMvc.put("/api/time-record/${createdRecord.id}/"){
            accept(MediaType.APPLICATION_JSON)
            contentType = MediaType.APPLICATION_JSON
            content = objectMapper.writeValueAsString(body)
        }.andExpect {
            status { isForbidden() }
        }
    }

    @Test
    fun `update time record should fail if the time record overlaps another time record`(){
        val employee = EntityGenerator.createEmployee()
        val createdEmployee = employeeRepository.save(employee)

        val timeRecord = EntityGenerator.createTimeRecord().copy(
            employee = createdEmployee
        )
        val createdRecord = timeRecordRepository.save(timeRecord)

        timeRecordRepository.save(createdRecord.copy(
            timeIn = LocalTime.parse("12:00:00"),
            timeOut = LocalTime.parse("15:00:00"),
        ))

        val body = createdRecord.copy(
            timeIn = LocalTime.parse("15:00:00"),
            timeOut = LocalTime.parse("16:00:00"),
            isUpdated = true,
            dateRecord = LocalDate.parse("2022-02-01")
        )

        mockMvc.put("/api/time-record/${createdRecord.id}/"){
            accept(MediaType.APPLICATION_JSON)
            contentType = MediaType.APPLICATION_JSON
            content = objectMapper.writeValueAsString(body)
        }.andExpect {
            status { isConflict() }
        }
    }
}