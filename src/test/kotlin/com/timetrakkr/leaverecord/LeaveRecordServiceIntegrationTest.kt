package com.timetrakkr.leaverecord

import com.fasterxml.jackson.databind.ObjectMapper
import com.timetrakkr.TimeTrakkrApplicationTests
import com.timetrakkr.entities.Employee
import com.timetrakkr.repositories.EmployeeRepository
import com.timetrakkr.repositories.LeaveRecordRepository
import com.timetrakkr.utils.EntityGenerator
import com.timetrakkr.utils.LeaveTypeEnum
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.http.MediaType
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.post
import org.springframework.test.web.servlet.put
import org.springframework.web.util.NestedServletException
import utils.EmployeeTypeEnum
import java.time.LocalDate
import kotlin.test.assertEquals
import kotlin.test.assertFailsWith

@AutoConfigureMockMvc
class LeaveRecordServiceIntegrationTest: TimeTrakkrApplicationTests() {
    @Autowired
    private lateinit var mockMvc: MockMvc

    @Autowired
    private lateinit var objectMapper: ObjectMapper

    @Autowired
    private lateinit var employeeRepository: EmployeeRepository

    @Autowired
    private lateinit var leaveRecordRepository: LeaveRecordRepository

    @BeforeEach
    fun setUp(){
        leaveRecordRepository.deleteAll()
        employeeRepository.deleteAll()
    }

    @Test
    fun `create should return a 200`(){
        val employee = EntityGenerator.createEmployee()
        val createdEmployee = employeeRepository.save(employee)


        val leaveRecord = EntityGenerator.createLeaveRecord().copy(
            leaveType = LeaveTypeEnum.VL.name,
            employee = createdEmployee
        )
        mockMvc.post("/api/leave-records/${createdEmployee.id}/"){
            accept(MediaType.APPLICATION_JSON)
            contentType = MediaType.APPLICATION_JSON
            content = objectMapper.writeValueAsString(leaveRecord)
        }.andExpect {
            status { isOk() }
        }
    }

    @Test
    fun `create should fail given a non existing employee id`(){
        val employee = EntityGenerator.createEmployee()
        val createdEmployee = employeeRepository.save(employee)

        val leaveRecord = EntityGenerator.createLeaveRecord().copy( employee = createdEmployee)

        val invalidId: Long = 123
        mockMvc.post("/api/leave-records/$invalidId/"){
            accept(MediaType.APPLICATION_JSON)
            contentType = MediaType.APPLICATION_JSON
            content = objectMapper.writeValueAsString(leaveRecord)
        }.andExpect {
            status { isNotFound() }
        }
    }

    @Test
    fun `create should fail if the leave is not filed 14 days in advance`(){
        val employee = EntityGenerator.createEmployee().copy()
        val createdEmployee = employeeRepository.save(employee)

        val leaveRecord = EntityGenerator.createLeaveRecord().copy(
            leaveType = LeaveTypeEnum.VL.name,
            dateRecord = LocalDate.now(),
            employee = createdEmployee
        )

        mockMvc.post("/api/leave-records/${createdEmployee.id}/"){
            accept(MediaType.APPLICATION_JSON)
            contentType = MediaType.APPLICATION_JSON
            content = objectMapper.writeValueAsString(leaveRecord)
        }.andExpect {
            status { isForbidden() }
        }
    }

    @Test
    fun `create should fail if there's no remaining vacation leave for the filed leave month`(){
        val employee = EntityGenerator.createEmployee()
        val createdEmployee = employeeRepository.save(employee)

        val leaveRecord = EntityGenerator.createLeaveRecord().copy( employee = createdEmployee )

        leaveRecordRepository.saveAll(
            listOf(
                leaveRecord,
                leaveRecord.copy( leaveType = LeaveTypeEnum.VL.value, dateRecord = LocalDate.parse("2022-02-02")),
                leaveRecord.copy( leaveType = LeaveTypeEnum.VL.value, dateRecord = LocalDate.parse("2022-02-03")),
                leaveRecord.copy( leaveType = LeaveTypeEnum.VL.value, dateRecord = LocalDate.parse("2022-02-04")),
                leaveRecord.copy( leaveType = LeaveTypeEnum.VL.value, dateRecord = LocalDate.parse("2022-02-05")),
            )
        )
        val body = leaveRecord.copy(
            leaveType = LeaveTypeEnum.VL.name,
            dateRecord = LocalDate.parse("2022-02-06")
        )

        mockMvc.post("/api/leave-records/${createdEmployee.id}/"){
            accept(MediaType.APPLICATION_JSON)
            contentType = MediaType.APPLICATION_JSON
            content = objectMapper.writeValueAsString(body)
        }.andExpect {
            status { isForbidden() }
        }
    }

    @Test
    fun `create should fail if there's no remaining sick leave for the filed leave month`(){
        val employee = EntityGenerator.createEmployee()
        val createdEmployee = employeeRepository.save(employee)

        val leaveRecord = EntityGenerator.createLeaveRecord().copy( employee = createdEmployee )

        leaveRecordRepository.saveAll(
            listOf(
                leaveRecord.copy( leaveType = LeaveTypeEnum.SL.value ),
                leaveRecord.copy( leaveType = LeaveTypeEnum.SL.value, dateRecord = LocalDate.parse("2022-02-02")),
            )
        )
        val body = leaveRecord.copy(
            leaveType = LeaveTypeEnum.SL.name,
            dateRecord = LocalDate.parse("2022-02-03")
        )
        mockMvc.post("/api/leave-records/${createdEmployee.id}/"){
            accept(MediaType.APPLICATION_JSON)
            contentType = MediaType.APPLICATION_JSON
            content = objectMapper.writeValueAsString(body)
        }.andExpect {
            status { isForbidden() }
        }
    }

    @Test
    fun `create should fail given an invalid leave type`(){
        val employee = EntityGenerator.createEmployee()
        val createdEmployee = employeeRepository.save(employee)

        val leaveRecord = EntityGenerator.createLeaveRecord().copy(
            leaveType = "Invalid leaveType",
            employee = createdEmployee
        )

        mockMvc.post("/api/leave-records/${createdEmployee.id}/") {
            accept(MediaType.APPLICATION_JSON)
            contentType = MediaType.APPLICATION_JSON
            content = objectMapper.writeValueAsString(leaveRecord)
        }.andExpect {
            status { isBadRequest() }
        }
    }

    @Test
    fun `approve leave should return updated isAprroved given valid inputs`(){
        val employee = EntityGenerator.createEmployee()
        val createdEmployee = employeeRepository.save(employee)

        val approver = employeeRepository.save(
            Employee(
            firstName = "Brenda",
            lastName = "Mage",
            employeeType = EmployeeTypeEnum.PM.value
            )
        )

        val leaveRecord = EntityGenerator.createLeaveRecord().copy( employee = createdEmployee)
        val createdLeaveRecord = leaveRecordRepository.save(leaveRecord)

        mockMvc.put("/api/leaveRecord/isApproved/${createdLeaveRecord.id}/${approver.id}/"){
            accept(MediaType.APPLICATION_JSON)
            contentType = MediaType.APPLICATION_JSON
            content = objectMapper.writeValueAsString(createdLeaveRecord)
        }.andExpect {
            status { isOk() }
        }
    }

    @Test
    fun `approve leave should fail given a non existing employee id`(){
        val employee = EntityGenerator.createEmployee()
        val createdEmployee = employeeRepository.save(employee)

        val leaveRecord = EntityGenerator.createLeaveRecord().copy( employee = createdEmployee)
        val createdLeaveRecord = leaveRecordRepository.save(leaveRecord)


        val invalidId: Long = 123

        mockMvc.put("/api/leaveRecord/isApproved/${createdLeaveRecord.id}/$invalidId/"){
            accept(MediaType.APPLICATION_JSON)
            contentType = MediaType.APPLICATION_JSON
            content = objectMapper.writeValueAsString(createdLeaveRecord)
        }.andExpect {
            status { isNotFound() }
        }
    }

    @Test
    fun `approve leave should fail given a non existing leave record id`(){
        val employee = EntityGenerator.createEmployee()
        val createdEmployee = employeeRepository.save(employee)

        val leaveRecord = EntityGenerator.createLeaveRecord().copy( employee = createdEmployee)
        val createdLeaveRecord = leaveRecordRepository.save(leaveRecord)

        val approver = employeeRepository.save(Employee(
            firstName = "Brenda",
            lastName = "Mage",
            employeeType = EmployeeTypeEnum.PM.value
        ))

        val invalidId: Long = 123
        mockMvc.put("/api/leaveRecord/isApproved/$invalidId/${approver.id}/"){
            accept(MediaType.APPLICATION_JSON)
            contentType = MediaType.APPLICATION_JSON
            content = objectMapper.writeValueAsString(createdLeaveRecord)
        }.andExpect {
            status { isNotFound() }
        }
    }

    @Test
    fun `approve leave should fail given a non-project manager approver`(){
        val employee = EntityGenerator.createEmployee()
        val createdEmployee = employeeRepository.save(employee)

        val approver = employeeRepository.save(Employee(
            firstName = "Brenda",
            lastName = "Mage",
            employeeType = EmployeeTypeEnum.C.value
        ))

        val leaveRecord = EntityGenerator.createLeaveRecord().copy( employee = createdEmployee)
        val createdLeaveRecord = leaveRecordRepository.save(leaveRecord)

        mockMvc.put("/api/leaveRecord/isApproved/${createdLeaveRecord.id}/${approver.id}/"){
            accept(MediaType.APPLICATION_JSON)
            contentType = MediaType.APPLICATION_JSON
            content = objectMapper.writeValueAsString(createdLeaveRecord)
        }.andExpect {
            status { isUnauthorized() }
        }

    }

    @Test
    fun `approve leave should fail if the approver is the one filing the leave`(){
        val employee = EntityGenerator.createEmployee().copy(employeeType = EmployeeTypeEnum.PM.value)
        val createdEmployee = employeeRepository.save(employee)

        val leaveRecord = EntityGenerator.createLeaveRecord().copy( employee = createdEmployee)
        val createdLeaveRecord = leaveRecordRepository.save(leaveRecord)
        mockMvc.put("/api/leaveRecord/isApproved/${createdLeaveRecord.id}/${employee.id}/"){
            accept(MediaType.APPLICATION_JSON)
            contentType = MediaType.APPLICATION_JSON
            content = objectMapper.writeValueAsString(createdLeaveRecord)
        }.andExpect {
            status { isForbidden() }
        }

    }

    @Test
    fun `approve leave should fail if the leave record was already approved`(){
        val employee = EntityGenerator.createEmployee()
        val createdEmployee = employeeRepository.save(employee)

        val approver = employeeRepository.save(Employee(
            firstName = "Brenda",
            lastName = "Mage",
            employeeType = EmployeeTypeEnum.PM.value
        ))

        val leaveRecord = EntityGenerator.createLeaveRecord().copy(
            isApproved = true,
            employee = createdEmployee
        )
        val createdLeaveRecord = leaveRecordRepository.save(leaveRecord)


        mockMvc.put("/api/leaveRecord/isApproved/${createdLeaveRecord.id}/${approver.id}/"){
            accept(MediaType.APPLICATION_JSON)
            contentType = MediaType.APPLICATION_JSON
            content = objectMapper.writeValueAsString(createdLeaveRecord)
        }.andExpect {
            status { isForbidden() }
        }
    }

}