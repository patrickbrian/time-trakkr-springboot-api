package com.timetrakkr.leaverecord

import com.timetrakkr.TimeTrakkrApplicationTests
import com.timetrakkr.entities.Employee
import com.timetrakkr.entities.LeaveRecord
import com.timetrakkr.repositories.EmployeeRepository
import com.timetrakkr.repositories.LeaveRecordRepository
import com.timetrakkr.serviceImpl.LeaveRecordServiceImpl
import com.timetrakkr.utils.EntityGenerator
import com.timetrakkr.utils.LeaveTypeEnum
import org.assertj.core.api.Assertions
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.server.ResponseStatusException
import utils.EmployeeTypeEnum
import java.time.LocalDate
import javax.transaction.Transactional
import kotlin.test.assertEquals
import kotlin.test.assertFailsWith

class LeaveRecordServiceImplTest: TimeTrakkrApplicationTests() {
    @Autowired
    private lateinit var employeeRepository: EmployeeRepository


    @Autowired
    private lateinit var leaveRecordRepository: LeaveRecordRepository

    @Autowired
    private lateinit var leaveRecordServiceImpl: LeaveRecordServiceImpl

    @BeforeEach
    fun setUp(){
        leaveRecordRepository.deleteAll()
        employeeRepository.deleteAll()
    }

    @Transactional
    @Test
    fun `create should return a leave record`(){

        val employee = EntityGenerator.createEmployee()
        val createdEmployee = employeeRepository.save(employee)

        assertEquals(0, leaveRecordRepository.findAll().count())
        val leaveRecord = EntityGenerator.createLeaveRecord().copy(
            leaveType = LeaveTypeEnum.VL.name,
            employee = createdEmployee
        )
        val createdLeaveRecord = leaveRecordServiceImpl.createLeaveRecord(leaveRecord, createdEmployee.id!!)

        leaveRecord.leaveType = LeaveTypeEnum.valueOf(leaveRecord.leaveType.uppercase()).value

        assertEquals(1, leaveRecordRepository.findAll().count())

        Assertions.assertThat( createdLeaveRecord )
            .usingRecursiveComparison()
            .ignoringFields(
                LeaveRecord::id.name
            )
            .withStrictTypeChecking()
            .isEqualTo(leaveRecord)

        assertEquals(leaveRecord.leaveType, createdLeaveRecord.leaveType)
        assertEquals(leaveRecord.dateRecord, createdLeaveRecord.dateRecord)
    }

    @Test
    fun `create should fail given a non existing employee id`(){
        val employee = EntityGenerator.createEmployee()
        val createdEmployee = employeeRepository.save(employee)

        val leaveRecord = EntityGenerator.createLeaveRecord().copy( employee = createdEmployee)

        val invalidId: Long = 123
        val exception = assertFailsWith<ResponseStatusException> {
            leaveRecordServiceImpl.createLeaveRecord(leaveRecord, invalidId)
        }

        val expectedException = "404 NOT_FOUND \"Employee with id: $invalidId does not exist.\""
        assertEquals(expectedException, exception.message)
    }

    @Test
    fun `create should fail if the leave is not filed 14 days in advance`(){
        val employee = EntityGenerator.createEmployee().copy()
        val createdEmployee = employeeRepository.save(employee)

        val leaveRecord = EntityGenerator.createLeaveRecord().copy(
            leaveType = LeaveTypeEnum.VL.name,
            dateRecord = LocalDate.now(),
            employee = createdEmployee
        )

        val exception = assertFailsWith<ResponseStatusException> {
            leaveRecordServiceImpl.createLeaveRecord(leaveRecord, createdEmployee.id!!)
        }

        val expectedException = "403 FORBIDDEN \"You can only file for a vacation leave 14 days in advance.\""
        assertEquals(expectedException, exception.message)

    }

    @Test
    fun `create should fail if there's no remaining vacation leave for the filed leave month`(){
        val employee = EntityGenerator.createEmployee()
        val createdEmployee = employeeRepository.save(employee)

        val leaveRecord = EntityGenerator.createLeaveRecord().copy( employee = createdEmployee )

        leaveRecordRepository.saveAll(
            listOf(
                leaveRecord,
                leaveRecord.copy( leaveType = LeaveTypeEnum.VL.value, dateRecord = LocalDate.parse("2022-02-02")),
                leaveRecord.copy( leaveType = LeaveTypeEnum.VL.value, dateRecord = LocalDate.parse("2022-02-03")),
                leaveRecord.copy( leaveType = LeaveTypeEnum.VL.value, dateRecord = LocalDate.parse("2022-02-04")),
                leaveRecord.copy( leaveType = LeaveTypeEnum.VL.value, dateRecord = LocalDate.parse("2022-02-05")),
            )
        )
        val exception = assertFailsWith<ResponseStatusException> {
            leaveRecordServiceImpl.createLeaveRecord(leaveRecord.copy(
                leaveType = LeaveTypeEnum.VL.name,
                dateRecord = LocalDate.parse("2022-02-06")
            ), createdEmployee.id!!)
        }

        val expectedException = "403 FORBIDDEN \"You already used your 5 vacation leave for the month of " +
                "${leaveRecord.dateRecord.month} ${leaveRecord.dateRecord.year}.\""
        assertEquals(expectedException, exception.message)
    }

    @Test
    fun `create should fail if there's no remaining sick leave for the filed leave month`(){
        val employee = EntityGenerator.createEmployee()
        val createdEmployee = employeeRepository.save(employee)

        val leaveRecord = EntityGenerator.createLeaveRecord().copy( employee = createdEmployee )

        leaveRecordRepository.saveAll(
            listOf(
                leaveRecord.copy( leaveType = LeaveTypeEnum.SL.value ),
                leaveRecord.copy( leaveType = LeaveTypeEnum.SL.value, dateRecord = LocalDate.parse("2022-02-02")),
            )
        )
        val exception = assertFailsWith<ResponseStatusException> {
            leaveRecordServiceImpl.createLeaveRecord(leaveRecord.copy(
                leaveType = LeaveTypeEnum.SL.name,
                dateRecord = LocalDate.parse("2022-02-03")
            ), createdEmployee.id!!)
        }

        val expectedException = "403 FORBIDDEN \"You already used your 2 sick leave for the month of " +
                "${leaveRecord.dateRecord.month} ${leaveRecord.dateRecord.year}.\""
        assertEquals(expectedException, exception.message)
    }

    @Test
    fun `create should fail given an invalid leave type`(){
        val employee = EntityGenerator.createEmployee()
        val createdEmployee = employeeRepository.save(employee)

        val leaveRecord = EntityGenerator.createLeaveRecord().copy(
            leaveType = "Invalid leaveType",
            employee = createdEmployee
        )

        val exception = assertFailsWith<ResponseStatusException> {
            leaveRecordServiceImpl.createLeaveRecord(leaveRecord, createdEmployee.id!!)
        }

        val expectedException = "400 BAD_REQUEST \"Leave type: ${leaveRecord.leaveType.uppercase()} " +
                "does not exist.\""

        assertEquals(expectedException, exception.message)
    }

    @Test
    fun `approve leave should return updated isAprroved given valid inputs`(){
        val employee = EntityGenerator.createEmployee()
        val createdEmployee = employeeRepository.save(employee)

        val approver = employeeRepository.save(Employee(
            firstName = "Brenda",
            lastName = "Mage",
            employeeType = EmployeeTypeEnum.PM.value
        ))

        val leaveRecord = EntityGenerator.createLeaveRecord().copy( employee = createdEmployee)
        val createdLeaveRecord = leaveRecordRepository.save(leaveRecord)


        val approvedLeaveRecord = leaveRecordServiceImpl.approveLeaveRecord(createdLeaveRecord.id!!, approver.id!!)

        assertEquals(true, approvedLeaveRecord.isApproved)
    }

    @Test
    fun `approve leave should fail given a non existing employee id`(){
        val employee = EntityGenerator.createEmployee()
        val createdEmployee = employeeRepository.save(employee)

        val leaveRecord = EntityGenerator.createLeaveRecord().copy( employee = createdEmployee)
        val createdLeaveRecord = leaveRecordRepository.save(leaveRecord)


        val invalidId: Long = 123
        val exception = assertFailsWith<ResponseStatusException> {
            leaveRecordServiceImpl.approveLeaveRecord(createdLeaveRecord.id!!, invalidId)
        }

        val expectedException = "404 NOT_FOUND \"Employee with id: $invalidId does not exist.\""
        assertEquals(expectedException, exception.message)

    }

    @Test
    fun `approve leave should fail given a non existing leave record id`(){
        val employee = EntityGenerator.createEmployee()
        val createdEmployee = employeeRepository.save(employee)

        val leaveRecord = EntityGenerator.createLeaveRecord().copy( employee = createdEmployee)
        leaveRecordRepository.save(leaveRecord)

        val approver = employeeRepository.save(Employee(
            firstName = "Brenda",
            lastName = "Mage",
            employeeType = EmployeeTypeEnum.PM.value
        ))

        val invalidId: Long = 123
        val exception = assertFailsWith<ResponseStatusException> {
            leaveRecordServiceImpl.approveLeaveRecord(invalidId, approver.id!!)
        }

        val expectedException = "404 NOT_FOUND \"Leave record with id: $invalidId does not exist.\""
        assertEquals(expectedException, exception.message)
    }

    @Test
    fun `approve leave should fail given a non-project manager approver`(){
        val employee = EntityGenerator.createEmployee()
        val createdEmployee = employeeRepository.save(employee)

        val approver = employeeRepository.save(Employee(
            firstName = "Brenda",
            lastName = "Mage",
            employeeType = EmployeeTypeEnum.C.value
        ))

        val leaveRecord = EntityGenerator.createLeaveRecord().copy( employee = createdEmployee)
        val createdLeaveRecord = leaveRecordRepository.save(leaveRecord)

        val exception = assertFailsWith<ResponseStatusException> {
            leaveRecordServiceImpl.approveLeaveRecord(createdLeaveRecord.id!!, approver.id!!)
        }

        val expectedException = "401 UNAUTHORIZED \"Only a Project Manager can approve a leave.\""
        assertEquals(expectedException, exception.message)
    }

    @Test
    fun `approve leave should fail if the approver is the one filing the leave`(){
        val employee = EntityGenerator.createEmployee().copy(employeeType = EmployeeTypeEnum.PM.value)
        val createdEmployee = employeeRepository.save(employee)

        val leaveRecord = EntityGenerator.createLeaveRecord().copy( employee = createdEmployee)
        val createdLeaveRecord = leaveRecordRepository.save(leaveRecord)

        val exception = assertFailsWith<ResponseStatusException> {
            leaveRecordServiceImpl.approveLeaveRecord(createdLeaveRecord.id!!, createdEmployee.id!!)
        }

        val expectedException = "403 FORBIDDEN \"You can not approve your own leave.\""
        assertEquals(expectedException, exception.message)
    }

    @Test
    fun `approve leave should fail if the leave record was already approved`(){
        val employee = EntityGenerator.createEmployee()
        val createdEmployee = employeeRepository.save(employee)

        val approver = employeeRepository.save(Employee(
            firstName = "Brenda",
            lastName = "Mage",
            employeeType = EmployeeTypeEnum.PM.value
        ))

        val leaveRecord = EntityGenerator.createLeaveRecord().copy(
            isApproved = true,
            employee = createdEmployee
        )
        val createdLeaveRecord = leaveRecordRepository.save(leaveRecord)

        val exception = assertFailsWith<ResponseStatusException> {
            leaveRecordServiceImpl.approveLeaveRecord(createdLeaveRecord.id!!, approver.id!!)
        }

        val expectedException = "403 FORBIDDEN \"Leave record with id: ${createdLeaveRecord.id} " +
                "was already approved.\""
        assertEquals(expectedException, exception.message)
    }

}