package com.timetrakkr.utils

import com.timetrakkr.entities.Employee
import com.timetrakkr.entities.LeaveRecord
import com.timetrakkr.entities.TimeRecord
import utils.EmployeeTypeEnum
import java.time.LocalDate
import java.time.LocalTime

object EntityGenerator {

    fun createEmployee(): Employee = Employee(
        firstName = "Brandon",
        lastName = "Cruz",
        employeeType = EmployeeTypeEnum.PM.name
    )

    fun createTimeRecord(): TimeRecord = TimeRecord(
        timeIn = LocalTime.parse("01:00:00"),
        timeOut = LocalTime.parse("11:00:00"),
        isUpdated = false,
        dateRecord = LocalDate.parse("2022-02-01")
    )

    fun createLeaveRecord(): LeaveRecord = LeaveRecord(
        leaveType = LeaveTypeEnum.VL.value,
        dateRecord = LocalDate.parse("2022-02-01")
    )
}