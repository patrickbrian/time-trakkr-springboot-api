package com.timetrakkr.employee

import com.timetrakkr.TimeTrakkrApplicationTests
import com.timetrakkr.entities.Employee
import com.timetrakkr.repositories.EmployeeRepository
import com.timetrakkr.serviceImpl.EmployeeServiceImpl
import com.timetrakkr.utils.EntityGenerator
import org.assertj.core.api.Assertions
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.server.ResponseStatusException
import utils.EmployeeTypeEnum
import kotlin.test.assertEquals
import kotlin.test.assertFailsWith

class EmployeeServiceImplTest: TimeTrakkrApplicationTests() {

    @Autowired
    private lateinit var employeeRepository: EmployeeRepository

    @Autowired
    private lateinit var employeeServiceImpl: EmployeeServiceImpl

    @BeforeEach
    fun setUp(){
        employeeRepository.deleteAll()
    }

    @Test
    fun `create should return an employee` (){
        val employee = EntityGenerator.createEmployee()
        val createdEmployee = employeeServiceImpl.createEmployee(employee)

        employee.employeeType = EmployeeTypeEnum.valueOf(employee.employeeType.uppercase()).value

        assertEquals(1, employeeRepository.findAll().count())

        Assertions.assertThat( createdEmployee )
            .usingRecursiveComparison()
            .ignoringFields(
                Employee::id.name
            )
            .withStrictTypeChecking()
            .isEqualTo(employee)

        assertEquals(employee.firstName, createdEmployee.firstName)
        assertEquals(employee.lastName, createdEmployee.lastName)
        assertEquals(employee.employeeType, createdEmployee.employeeType)
    }

    @Test
    fun `create should fail given duplicate combination of first and last name`(){
        val employee = EntityGenerator.createEmployee()
        employeeRepository.save(employee)

        val duplicateEmployee = employee.copy(
            firstName = "Brandon",
            lastName = "Cruz"
        )

        val exception = assertFailsWith<ResponseStatusException> {
            employeeServiceImpl.createEmployee(duplicateEmployee)
        }
        val expectedException = "409 CONFLICT \"The name: ${employee.firstName} ${employee.lastName} already exists!\""
        assertEquals(expectedException, exception.message)
    }

    @Test
    fun `create should fail given an invalid employee type`(){
        val employee = EntityGenerator.createEmployee().copy(
            employeeType = "Invalid employeeType"
        )

        val exception = assertFailsWith<ResponseStatusException>{
            employeeServiceImpl.createEmployee(employee)
        }

        val expectedException = "400 BAD_REQUEST \"Employee type: ${employee.employeeType.uppercase()} " +
                "does not exist.\""

        assertEquals(expectedException, exception.message)
    }

    @Test
    fun `update employee should return updated given valid inputs`(){
        val employee = EntityGenerator.createEmployee()
        val originalEmployee = employeeRepository.save(employee)

        val body = originalEmployee.copy(
            firstName = "Brenda",
            lastName = "Mage",
        )
        val updatedEmployee = employeeServiceImpl.updateEmployee(body, originalEmployee.id!!)

        assertEquals(body.firstName, updatedEmployee.firstName)
        assertEquals(body.lastName, updatedEmployee.lastName)
    }

    @Test
    fun `update should fail given a non existing employee id`() {
        val invalidId: Long = 123

        val employee = EntityGenerator.createEmployee()
        val originalEmployee = employeeRepository.save(employee)

        val body = originalEmployee.copy(
            firstName = "Brenda",
            lastName = "Mage",
        )

        val exception = assertFailsWith<ResponseStatusException> {
            employeeServiceImpl.updateEmployee(body, invalidId)
        }

        val expectedException = "404 NOT_FOUND \"Employee with id: $invalidId does not exist.\""
        assertEquals(expectedException, exception.message)
    }

    @Test
    fun `update should fail given duplicate combination of first and last name`(){
        val employee = EntityGenerator.createEmployee()
        val originalEmployee = employeeRepository.save(employee)

        val body = originalEmployee.copy(
            firstName = "Brandon",
            lastName = "Cruz",
        )

        val exception = assertFailsWith<ResponseStatusException> {
            employeeServiceImpl.updateEmployee(body, originalEmployee.id!!)
        }

        val expectedException = "409 CONFLICT \"The name: ${employee.firstName} ${employee.lastName} already exists!\""
        assertEquals(expectedException, exception.message)

    }

    @Test
    fun `updating isActive attribute should return updated given valid inputs`(){
        val employee = EntityGenerator.createEmployee()
        val originalEmployee = employeeRepository.save(employee)

        val updatedUser = employeeServiceImpl.toggleIsActive(originalEmployee.id!!)

        assertEquals(false, updatedUser.isActive)
    }

    @Test
    fun `updating isActive attribute should fail given a non existing employee id`(){
        val invalidId: Long = 123
        val exception = assertFailsWith<ResponseStatusException> {
            employeeServiceImpl.toggleIsActive(invalidId)
        }

        val expectedException = "404 NOT_FOUND \"Employee with id: $invalidId does not exist.\""
        assertEquals(expectedException, exception.message)

    }


}