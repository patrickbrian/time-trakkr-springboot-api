package com.timetrakkr.employee

import com.fasterxml.jackson.databind.ObjectMapper
import com.timetrakkr.TimeTrakkrApplicationTests
import com.timetrakkr.repositories.EmployeeRepository
import com.timetrakkr.repositories.LeaveRecordRepository
import com.timetrakkr.repositories.TimeRecordRepository
import com.timetrakkr.utils.EntityGenerator

import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.http.MediaType
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.post
import org.springframework.test.web.servlet.put
import org.springframework.web.util.NestedServletException
import kotlin.test.assertEquals
import kotlin.test.assertFailsWith

@AutoConfigureMockMvc
class EmployeeServiceIntegrationTest: TimeTrakkrApplicationTests() {

    @Autowired
    private lateinit var mockMvc: MockMvc

    @Autowired
    private lateinit var objectMapper: ObjectMapper

    @Autowired
    private lateinit var employeeRepository: EmployeeRepository

    @Autowired
    private lateinit var timeRecordRepository: TimeRecordRepository

    @Autowired
    private lateinit var leaveRecordRepository: LeaveRecordRepository

    @BeforeEach
    fun setUp(){
        leaveRecordRepository.deleteAll()
        timeRecordRepository.deleteAll()
        employeeRepository.deleteAll()

    }

    @Test
    fun `create employee should return 200`() {
        val body = EntityGenerator.createEmployee()

        mockMvc.post("/api/employees/"){
            accept(MediaType.APPLICATION_JSON)
            contentType = MediaType.APPLICATION_JSON
            content = objectMapper.writeValueAsString(body)
        }.andExpect {
            status { isOk() }
        }
    }

    @Test
    fun `create should fail given duplicate combination of first and last name`(){

        val employee = EntityGenerator.createEmployee()
        employeeRepository.save(employee)

        val duplicateEmployee = employee.copy(
            firstName = "Brandon",
            lastName = "Cruz"
        )

        mockMvc.post("/api/employees/"){
            accept(MediaType.APPLICATION_JSON)
            contentType = MediaType.APPLICATION_JSON
            content = objectMapper.writeValueAsString(duplicateEmployee)
        }.andExpect {
            status { isConflict() }
        }
    }


    @Test
    fun `create should fail given an invalid employee type`(){
        val employee = EntityGenerator.createEmployee().copy(
            employeeType = "Invalid employeeType"
        )

        mockMvc.post("/api/employees/") {
            accept(MediaType.APPLICATION_JSON)
            contentType = MediaType.APPLICATION_JSON
            content = objectMapper.writeValueAsString(employee)
        }.andExpect {
            status { isBadRequest() }
        }

    }

    @Test
    fun `update employee should return updated given valid inputs`(){
        val employee = EntityGenerator.createEmployee()
        val originalEmployee = employeeRepository.save(employee)

        val body = originalEmployee.copy(
            firstName = "Brenda",
            lastName = "Mage",
        )

        mockMvc.put("/api/employee/${originalEmployee.id}/"){
            accept(MediaType.APPLICATION_JSON)
            contentType = MediaType.APPLICATION_JSON
            content = objectMapper.writeValueAsString(body)
        }.andExpect {
            status { isOk() }
        }
    }

    @Test
    fun `update should fail given a non existing employee id`(){
        val invalidId: Long = 123

        val employee = EntityGenerator.createEmployee()
        val originalEmployee = employeeRepository.save(employee)

        val body = originalEmployee.copy(
            firstName = "Brenda",
            lastName = "Mage",
        )

        mockMvc.put("/api/employee/${invalidId}/"){
            accept(MediaType.APPLICATION_JSON)
            contentType = MediaType.APPLICATION_JSON
            content = objectMapper.writeValueAsString(body)
        }.andExpect {
            status { isNotFound() }
        }
    }

    @Test
    fun `update should fail given duplicate combination of first and last name`(){
        val employee = EntityGenerator.createEmployee()
        val originalEmployee = employeeRepository.save(employee)

        val body = originalEmployee.copy(
            firstName = "Brandon",
            lastName = "Cruz",
        )

        mockMvc.put("/api/employee/${originalEmployee.id}/"){
            accept(MediaType.APPLICATION_JSON)
            contentType = MediaType.APPLICATION_JSON
            content = objectMapper.writeValueAsString(body)
        }.andExpect {
            status { isConflict() }
        }
    }

    @Test
    fun `updating isActive attribute should return updated given valid inputs`(){
        val employee = EntityGenerator.createEmployee()
        val originalEmployee = employeeRepository.save(employee)

        mockMvc.put("/api/employee/${originalEmployee.id}/isActive/"){
            accept(MediaType.APPLICATION_JSON)
            contentType = MediaType.APPLICATION_JSON
        }.andExpect {
            status { isOk() }
        }
    }

    @Test
    fun `updating isActive attribute should fail given a non existing employee id`(){
        val invalidId: Long = 123

        mockMvc.put("/api/employee/${invalidId}/isActive/"){
            accept(MediaType.APPLICATION_JSON)
            contentType = MediaType.APPLICATION_JSON
        }.andExpect {
            status { isNotFound() }
        }
    }
}